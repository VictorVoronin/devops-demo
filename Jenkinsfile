pipeline
        {
            agent any
            tools {
                maven 'Maven'
            }

            stages
                    {
                        stage('Build')
                                {
                                    steps {
                                        git 'https://github.com/AlexanderLazarev/spring-petclinic.git'
                                        sh 'mvn package -DskipTests=true'
                                    }
                                }

                        stage('Test')
                                {
                                    steps {
                                        input message: 'Сборка прошла успешно. Продолжить?', ok: 'Ok'
                                        sh 'mvn test'
                                        script {
                                            junit 'target/*-reports/*.xml, */target/*-reports/*.xml'
                                        }
                                    }
                                }

                        stage('Code analysis')
                                {
                                    steps {
                                        input message: 'Тесты прошли успешно. Продолжить?', ok: 'Ok'
                                        script {
                                            def scannerHome = tool 'SonarScanner';
                                            withSonarQubeEnv('SonarServer')
                                                    {
                                                        sh "mvn sonar:sonar -Dsonar.projectKey=opendays "
                                                    }
                                            sleep(10)
                                            timeout(time: 1, unit: 'MINUTES')
                                                    {
                                                        withSonarQubeEnv('SonarServer')
                                                                {
                                                                    def qp = waitForQualityGate()
                                                                    print "Finished waiting"
                                                                    if (qp.status != "OK")
                                                                        error "Такой код нам не нужен!"
                                                                }
                                                    }
                                        }
                                    }
                                }

                        stage('Package to container')
                                {
                                    steps {
                                        input message: 'Проверка кода прошла успешно. Продолжить?', ok: 'Ok'
                                        withCredentials([usernamePassword(credentialsId: 'quay', passwordVariable: 'password', usernameVariable: 'username')]) {
                                            sh "docker build --build-arg JAR_FILE=\"target/spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar\" -t quay.io/${username}/devops-demo:${BUILD_NUMBER} ."
                                        }
                                    }
                                }

                        stage('Push to Docker registry')
                                {
                                    steps {
                                        input message: 'Упаковка в контейнер прошла успешно. Продолжить?', ok: 'Ok'
                                        withCredentials([usernamePassword(credentialsId: 'quay', passwordVariable: 'password', usernameVariable: 'username')]) {
                                            sh """
                       docker login -u ${username} -p ${password} quay.io
                       docker push quay.io/${username}/devops-demo:${BUILD_NUMBER}
                      """
                                        }
                                    }
                                }

                        stage('Deploy')
                                {
                                    steps {
                                        input message: 'Перенос в Docker registry прошел успешно. Продолжить?', ok: 'Ok'
                                        script {
                                            withCredentials([usernamePassword(credentialsId: 'quay', passwordVariable: 'password', usernameVariable: 'username')]) {
                                                //sh 'docker ps -aq --filter ancestor=quay.io/devops-demo/devops-demo | xargs --no-run-if-empty docker rm -f'
                                                sh "docker ps | awk '/devops-demo/{print \$1}' | xargs --no-run-if-empty docker stop"
                                                sh 'docker run -d -p 80:8080 quay.io/${username}/devops-demo:${BUILD_NUMBER}'
                                                print 'УСТАНОВКА ПРОШЛА УСПЕШНО!'
                                            }
                                        }
                                    }
                                }
                    }
        }
