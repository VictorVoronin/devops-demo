resource "google_compute_firewall" "fw-rule-jenkins" {
  name        = "allow-jenkins-ui"
  network     = "default"
  description = "Creates Firewall rule targetting tagged instances"

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }

  target_tags   = ["jenkins"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "fw-rule-sonar" {
  name        = "allow-sonar-ui"
  network     = "default"
  description = "Creates Firewall rule targetting tagged instances"

  allow {
    protocol = "tcp"
    ports    = ["9000"]
  }

  target_tags   = ["sonar"]
  source_ranges = ["0.0.0.0/0"]
}

