resource "google_compute_global_forwarding_rule" "jenkins" {
  name       = "jenkins-ui"
  target     = google_compute_target_http_proxy.jenkins.self_link
  port_range = "80"
}

resource "google_compute_target_http_proxy" "jenkins" {
  name        = "target-jenkins"
  description = "a description"
  url_map     = "${google_compute_url_map.jenkins_map.self_link}"
}

resource "google_compute_url_map" "jenkins_map" {
  name            = "jenkins-lb"
  description     = "a description"
  default_service = "${google_compute_backend_service.jenkins.self_link}"
}

resource "google_compute_backend_service" "jenkins" {
  project = var.project

  name        = "jenkins-ui"
  description = "Jenkins WEB UI"
  port_name   = "jenkins"
  protocol    = "HTTP"
  timeout_sec = 10
  enable_cdn  = false

  backend {
    group = google_compute_instance_group.application.self_link
  }

  health_checks = [google_compute_health_check.jenkins.self_link]
  depends_on    = [google_compute_instance_group.application]
}

resource "google_compute_health_check" "jenkins" {
  project = var.project
  name    = "jenkins-health"

  http_health_check {
    port         = 8080
    request_path = "/login"
  }

  check_interval_sec = 5
  timeout_sec        = 5
}

resource "google_compute_target_https_proxy" "jenkins" {
  count       = var.enable_ssl ? 1 : 0
  name             = "target-jenkins-https"
  url_map          = "${google_compute_url_map.jenkins_map.self_link}"
  ssl_certificates = ["${google_compute_ssl_certificate.default[0].self_link}"]
}

resource "google_compute_global_forwarding_rule" "jenkins-https" {
  count       = var.enable_ssl ? 1 : 0
  name       = "jenkins-ui-https"
  target     = google_compute_target_https_proxy.jenkins[0].self_link
  port_range = "443"
}
