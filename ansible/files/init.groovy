#!groovy

/*
 * This script is designated for the init.groovy.d 
 * directory to be executed at startup time of the 
 * Jenkins instance. This script requires the jobDSL
 * Plugin. Tested with job-dsl:1.70
 */

import jenkins.model.Jenkins
import hudson.security.FullControlOnceLoggedInAuthorizationStrategy
import hudson.security.HudsonPrivateSecurityRealm
import jenkins.install.*
import hudson.plugins.sonar.*
import hudson.plugins.sonar.model.*
import hudson.triggers.*


// Disable Setup Wizards
def JENKINS_HOME = '/var/lib/jenkins'
file = new File("${JENKINS_HOME}/done-config")
if (file.exists()) {
  println("[DEBUG] Configuration of Jenkins has been already done")
  return
}

println("[INFO] Configuration started")

def instance = Jenkins.getInstance()

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
instance.setSecurityRealm(hudsonRealm)
def user = instance.getSecurityRealm().createAccount("admin", "password")
user.save()

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
strategy.setAllowAnonymousRead(false)
instance.setAuthorizationStrategy(strategy)
instance.setInstallState(InstallState.INITIAL_SETUP_COMPLETED)
instance.save()

println("[INFO] SetupWizard Disabled")

String filename = "${JENKINS_HOME}/done-config"
println("[INFO] Createing file - ${filename}")
new File(filename).createNewFile()
println("[INFO] File created")

println("[INFO] Restart initialized")
Jenkins.instance.restart()
