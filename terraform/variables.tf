// Global variables
variable "gce_ssh_pub_key_file" {
  default = "id_rsa_gcp.pub"
}

variable "credentials" {
  default = ""
}

variable "gcp_region" {
  default = ""
}

variable "gcp_zone" {
  default = ""
}

variable "dns_name" {
  default = "devops-demo.gq"
}

variable "project" {
  default = "devops-demo-252018"
}

variable "enable_ssl" {
  default = false
}

variable "ssl_crt" {
  default = ""
}

variable "ssl_key" {
  default = ""
}

variable "ssl_inter_crt" {
  default = ""
}