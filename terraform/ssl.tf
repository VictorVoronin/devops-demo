resource "google_compute_ssl_certificate" "default" {
  count       = var.enable_ssl ? 1 : 0
  name_prefix = "test"
  description = "Default wildcard ssl"
  private_key = "${file("${var.ssl_key}")}"
  certificate = "${file("${var.ssl_crt}")}"

  lifecycle {
    create_before_destroy = true
  }
}
