resource "google_compute_instance" "jenkins_instance" {
  name         = "ci-instance"
  machine_type = "custom-4-16384"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = 20
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network = "default"

    access_config {
      nat_ip = google_compute_address.external_ip.address
    }
  }

  metadata = {
    sshKeys = "devops:${file(var.gce_ssh_pub_key_file)}"
  }

  tags = ["jenkins", "sonar", "http-server", "https-server"]
}

resource "google_compute_address" "external_ip" {
  name = "master-address"
}

resource "google_compute_instance_group" "application" {
  name      = "application"
  instances = ["${google_compute_instance.jenkins_instance.self_link}"]
  zone      = "${var.gcp_zone}"

  named_port {
    name = "jenkins"
    port = 8080
  }

  named_port {
    name = "sonar"
    port = 9000
  }

  named_port {
    name = "application"
    port = 80
  }
}