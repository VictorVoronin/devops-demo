output "instance_external_ip" {
  value = google_compute_address.external_ip.address
}

output "jenkins_lb_ip" {
  value = google_compute_global_forwarding_rule.jenkins.ip_address
}

output "sonar_lb_ip" {
  value = google_compute_global_forwarding_rule.sonar.ip_address
}