Install demo environment

1. Register free DNS record on freenom.com, for example devops-demo.gq
2. Create project in GCP
3. Create credentials for API access in corresponding project and save credentials JSON
4. Create ssh key pair for use in this project
5. Create tfvars file with project parameters (example provided)
6. Run terraform plan, terraform apply
7. Get "instance_external_ip" to use in Ansible
8. Check DNS nameserver and set them as nameservers in your freenom DNS
