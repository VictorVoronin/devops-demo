#!groovy

/*
 * This script is designated for the init.groovy.d 
 * directory to be executed at startup time of the 
 * Jenkins instance. This script requires the jobDSL
 * Plugin. Tested with job-dsl:1.70
 */

import jenkins.model.Jenkins
import hudson.security.FullControlOnceLoggedInAuthorizationStrategy
import hudson.security.HudsonPrivateSecurityRealm
import jenkins.install.*

// Disable Setup Wizards
    def JENKINS_HOME = '/var/lib/jenkins'
    file = new File("${JENKINS_HOME}/done-config")
    if (!file.exists()) {
      println("[DEBUG] Configuration of Jenkins has not been already done")
      return
    }

    file = new File("${JENKINS_HOME}/done-plugins")
    if (file.exists()) {
      println("[DEBUG] Plugins installation has been already done")
      return
    }

    println("Plugins installation started")

    def instance = Jenkins.getInstance()

/* Install required plugins and restart Jenkins, if necessary.  */

final List<String> REQUIRED_PLUGINS = [
    "copyartifact",
    "git",
    "ssh-agent",
    "tap",
    "workflow-aggregator",
    "sonar",
    "plain-credentials",
    "cloudbees-folder",
    "workflow-aggregator",
]

 REQUIRED_PLUGINS.each() { it ->
        Jenkins.instance.updateCenter.getPlugin(it).deploy()
        println("[INFO] Plugin ${it} installed")
    }

    println("Plugins installed")

    String filename = "${JENKINS_HOME}/done-plugins"
    println("Createing file - ${filename}")
    new File(filename).createNewFile()
    println("File creates")

    println("Restart initialized")
//    Jenkins.instance.restart()
    Jenkins.instance.doSafeRestart(null)
