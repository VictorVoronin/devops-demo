gce_ssh_pub_key_file = "/home/victor_voronin_gcp/.ssh/id_rsa_gcp.pub"
credentials          = "/home/victor_voronin_gcp/gcp/devops-key.json"
gcp_region           = "us-east1"
gcp_zone             = "us-east1-b"
dns_name             = "devops-demo.gq"
project              = "devops-demo-252018"

enable_ssl    = true
ssl_crt       = "/home/victor_voronin_gcp/ssl/devops-demo.crt"
ssl_key       = "/home/victor_voronin_gcp/ssl/devops-demo.key"
ssl_inter_crt = "/home/victor_voronin_gcp/ssl/devops-demo.inter"
