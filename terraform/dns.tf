resource "google_dns_managed_zone" "base_zone" {
  name     = "base-zone"
  dns_name = "${var.dns_name}."
}

resource "google_dns_record_set" "application" {
  name = "${var.dns_name}."
  type = "A"
  ttl  = 30

  managed_zone = "${google_dns_managed_zone.base_zone.name}"
  rrdatas      = ["${google_compute_address.external_ip.address}"]
}

resource "google_dns_record_set" "jenkins" {
  name = "jenkins.${var.dns_name}."
  type = "A"
  ttl  = 30

  managed_zone = "${google_dns_managed_zone.base_zone.name}"
  rrdatas      = ["${google_compute_global_forwarding_rule.jenkins.ip_address}"]
}

resource "google_dns_record_set" "sonar" {
  name = "sonar.${var.dns_name}."
  type = "A"
  ttl  = 30

  managed_zone = "${google_dns_managed_zone.base_zone.name}"
  rrdatas      = ["${google_compute_global_forwarding_rule.sonar.ip_address}"]
}