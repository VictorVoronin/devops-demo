resource "google_compute_global_forwarding_rule" "sonar" {
  name       = "sonar-ui"
  target     = google_compute_target_http_proxy.sonar.self_link
  port_range = "80"
}

resource "google_compute_target_http_proxy" "sonar" {
  name        = "target-sonar"
  description = "a description"
  url_map     = "${google_compute_url_map.sonar_map.self_link}"
}

resource "google_compute_url_map" "sonar_map" {
  name            = "sonar-lb"
  description     = "a description"
  default_service = "${google_compute_backend_service.sonar.self_link}"
}

resource "google_compute_backend_service" "sonar" {
  project = var.project

  name        = "sonar-ui"
  description = "Sonar WEB UI"
  port_name   = "sonar"
  protocol    = "HTTP"
  timeout_sec = 10
  enable_cdn  = false

  backend {
    group = google_compute_instance_group.application.self_link
  }

  health_checks = [google_compute_health_check.sonar.self_link]
  depends_on    = [google_compute_instance_group.application]
}

resource "google_compute_health_check" "sonar" {
  project = var.project
  name    = "sonar-health"

  http_health_check {
    port         = 9000
    request_path = "/"
  }

  check_interval_sec = 5
  timeout_sec        = 5
}

