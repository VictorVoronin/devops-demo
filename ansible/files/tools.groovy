import jenkins.model.Jenkins
import hudson.security.FullControlOnceLoggedInAuthorizationStrategy
import hudson.security.HudsonPrivateSecurityRealm
import jenkins.install.*
import hudson.plugins.sonar.*
import hudson.plugins.sonar.model.*
import hudson.triggers.*
import hudson.tools.InstallSourceProperty
import hudson.model.Hudson
import hudson.util.Secret
import hudson.tasks.Maven.MavenInstallation
import hudson.tools.ToolProperty
import hudson.tools.ToolPropertyDescriptor
import hudson.util.DescribableList
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition
import org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition
import hudson.plugins.git.GitSCM

// Disable Setup Wizards
def JENKINS_HOME = '/var/lib/jenkins'
file = new File("${JENKINS_HOME}/done-plugins")
if (!file.exists()) {
  println("[DEBUG] Configuration of Jenkins plugins has not been already done")
  return
}

file = new File("${JENKINS_HOME}/done-tools")
if (file.exists()) {
  println("[DEBUG] Configuration of Jenkins tools has been already done")
  return
}

println("[INFO] Configuration started")

def instance = Jenkins.getInstance()

SonarGlobalConfiguration sonarConf = Hudson.instance.getDescriptorByType(SonarGlobalConfiguration.class)
/*def sonarProperties = new SonarInstallation(
        "Sonar",
        "http://localhost:9000",
        "5.3",
        "",
        new TriggersConfig(),
        ""
)*/

def sonarProperties = new SonarInstallation(
        "SonarServer",
        "http://localhost:9000",
        null,
        null,
        null,
        "",
        "",
        "",
        new TriggersConfig()
)

sonarConf.setInstallations(sonarProperties)
sonarConf.save()

def sonarDescriptor = Jenkins.instance.getDescriptor("hudson.plugins.sonar.SonarRunnerInstallation")

def sonarRunnerInstaller = new SonarRunnerInstaller("3.1.0.1141")
def installSourceProperty = new InstallSourceProperty([sonarRunnerInstaller])
def sonarRunnerInstance = new SonarRunnerInstallation("SonarScanner", "", [installSourceProperty])

def sonarRunnerInstallations = sonarDescriptor.getInstallations()
sonarRunnerInstallations += sonarRunnerInstance
sonarDescriptor.setInstallations((SonarRunnerInstallation[]) sonarRunnerInstallations)
sonarDescriptor.save()

// Maven configuration
def mavenDesc = jenkins.model.Jenkins.instance.getExtensionList(hudson.tasks.Maven.DescriptorImpl.class)[0]

def isp = new InstallSourceProperty()
def autoInstaller = new hudson.tasks.Maven.MavenInstaller("3.6.2")
isp.installers.add(autoInstaller)

def proplist = new DescribableList<ToolProperty<?>, ToolPropertyDescriptor>()
proplist.add(isp)

def installation = new MavenInstallation("Maven", "", proplist)

mavenDesc.setInstallations(installation)
mavenDesc.save()

// Create job
def project = Jenkins.instance.createProject(WorkflowJob.class, "Application")
project.setDefinition(new CpsScmFlowDefinition(new GitSCM("https://VictorVoronin@bitbucket.org/VictorVoronin/devops-demo.git"), "Jenkinsfile"))
project.save()

println("[INFO] SetupWizard Disabled")

String filename = "${JENKINS_HOME}/done-tools"
println("[INFO] Createing file - ${filename}")
new File(filename).createNewFile()
println("[INFO] File created")

println("[INFO] Restart initialized")
Jenkins.instance.doSafeRestart(null)
